# Evolves Kubernetes Helm Charts

[![Artifact HUB](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/evolves&style=for-the-badge)](https://artifacthub.io/packages/search?repo=evolves)

The code is provided as-is with no warranties.

## Charts

- [Backup](./backup)

## Usage

### Add repository

```shell
helm repo add evolves http://charts.evolves.fr
```

### Search into repository

```shell
helm search repo evolves
```
