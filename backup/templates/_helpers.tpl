{{/* vim: set filetype=mustache: */}}

{{/* Expand the name of the chart. */}}
{{- define "backup.name" -}}
{{- if eq .Chart.Name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name .Chart.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
