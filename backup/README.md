# Backup

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/evolves-fr/backup?style=for-the-badge)](https://goreportcard.com/report/gitlab.com/evolves-fr/backup)
[![Maturity](https://img.shields.io/badge/Maturity-Alpha-red?style=for-the-badge)]()
[![License](https://img.shields.io/badge/license-MIT-brightgreen?style=for-the-badge)](https://gitlab.com/evolves-fr/backup/blob/master/LICENSE.md)

[![Version](https://img.shields.io/badge/dynamic/yaml?style=for-the-badge&label=Version&query=entries.backup[0].version&url=http%3A%2F%2Fcharts.evolves.fr%2F)](https://artifacthub.io/packages/helm/evolves/backup)
[![AppVersion](https://img.shields.io/badge/dynamic/yaml?style=for-the-badge&label=App%20Version&query=entries.backup[0].appVersion&url=http%3A%2F%2Fcharts.evolves.fr%2F)](https://gitlab.com/evolves-fr/backup/-/releases)
[![Artifact HUB](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/evolves&style=for-the-badge)](https://artifacthub.io/packages/search?repo=evolves)

*Backup* is an open-source tools written in Go (Golang). Create backup from multi sources, to multi destinations.


## Introduction

This chart bootstraps a backup cronjob on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Installing the Chart

To install the chart with the release name `backup` run:

```bash
$ helm repo add evolves http://charts.evolves.fr
$ helm install backup evolves/backup
```

The command deploys backup on the Kubernetes cluster using the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

## Uninstalling the Chart

To uninstall the `backup` deployment run:

```bash
$ helm uninstall backup
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration

Please see the [values schema reference documentation in Artifact Hub](https://artifacthub.io/packages/helm/evolves/backup?modal=values-schema) for a list of the configurable parameters of the chart and their default values.

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```bash
$ helm install backup \
  --set cronjob.schedule="0 2 * * *" \
  evolves/backup
```

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```bash
$ helm install backup -f values.yaml evolves/backup
```
