clean:
	rm -rf .build

build: build-backup

build-backup: clean
	helm package ./backup --destination .build

sign:
	helm gpg sign -u 4903C52D .build/*.tgz

push: sign
	helm bucket push --bucket charts.evolves.fr --url http://charts.evolves.fr .build
